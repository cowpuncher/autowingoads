## Step 1 - verify Java installation on your machine

If you don't have Java installed (in order to verify need execute command 'java -version' in terminal or command line on your PC), 
install the Java Software Development Kit (SDK) from http://www.oracle.com/technetwork/java/javase/downloads/index.html. 
You need install version not less than 1.8.

## Step 2: Set JAVA environment

Set the JAVA_HOME environment variable to point to the base directory location where Java is installed on your machine. For example:
windows - environment variable JAVA_HOME to C:\Program Files\Java\jdk1.8.x_xx
linux -	export JAVA_HOME=/usr/local/java-current
mac os - export JAVA_HOME=/Library/Java/Home

Verify Java installation using '$ java -version' command.

## Step 3: Download Maven archive

Go to https://maven.apache.org/download.cgi and download binary archive. After download extract all files to your PC in some directory (for example /user/user_name/maven).

## Step 4: Set M2_HOME and M2 environment

windows:
environment variable M2_HOME to C:\Program Files\maven-3.x.x
linux, mac os: 
export M2_HOME=/usr/user_name/maven-3.x.x
export M2=$M2_HOME/bin

Verify Maven installation using '$ mvn -version' command.

## Step 5: Download project, set configurations and run tests
All test(UI/API) storage in: 
"src/main/test/java".

$ cd /usr/user_name/tests_project

In order to run tests from MAVEN need execute below commands in console(terminal):

$ mvn clean -Dtest=NameOfClass test -Dselenide.browser=NameOfBrowser

** if you want to execute test from IDEA, uncomment line "//Configuration.browser = NameOfBrowser;" in BrowserConfiguration.class.
