package com.wingoads.utils.db.redis;

import redis.clients.jedis.Jedis;

/**
 * Created by dendy on 1/13/17.
 */
public class RedisCleaner {

    String host = "10.10.10.30";
    int port = 6379;
    int dbIndex = 5;


    public void clearRedis() {
        Jedis jedis = new Jedis(host, port);
        jedis.connect();
        System.out.println("connect to reedis.");
        jedis.select(dbIndex);
        System.out.println("select dbIndex.");
        jedis.flushDB();
        System.out.println("executed flushDB.");
        jedis.close();
        System.out.println("closed Redis connection.");
    }
}
