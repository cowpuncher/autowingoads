package com.wingoads.utils.db.mysql;


import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.*;

/**
 * Created by mario on 7/18/17.
 */
public class Mysql {
    public static String getActivationLink(String email) throws ParserConfigurationException, IOException, SQLException, ClassNotFoundException {
        String username = "root";
        String password = "mysql_root_password123";
        String connectionUrl = "jdbc:mysql://wingoads.com:3306/";
        String databaseName = "proj_database";

//         mate universal
        String searchPhrase = "http://wingoads.com/register/confirm/";


        Class.forName("com.mysql.cj.jdbc.Driver");

        String activationLink = null;
        try (Connection conn = DriverManager.getConnection(connectionUrl + databaseName, username, password)) {
            System.out.println("we're connected");

            Statement statement = conn.createStatement();

            while (activationLink == null) {
                ResultSet getActivationToken = statement.executeQuery("select confirmation_token from fos_user_user where email ='" + email + "'");

                while (getActivationToken.next()) {

                    String activationToken = getActivationToken.getString(1);
                    System.out.println(activationToken);
                    activationLink = activationToken;
                    System.out.println(activationLink);
                }
            }
        }
        return activationLink;
    }

}
