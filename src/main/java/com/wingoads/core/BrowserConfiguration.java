package com.wingoads.core;

import com.codeborne.selenide.Configuration;
import com.wingoads.core.common.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.WebDriverRunner.CHROME;

/**
 * Created by mario on 7/17/17.
 */
@Listeners(com.codeborne.selenide.testng.TextReport.class)
public class BrowserConfiguration extends BaseTest{

    @BeforeClass
    public void startServer() throws Exception {

//      System.setProperty("webdriver.chrome.driver", "src/main/resources/webDriver/chrome/mac64/chromedriver");
//      System.setProperty("webdriver.gecko.driver", "src/main/resources/webDriver/firefox/mac64/geckodriver");
//      Configuration.browser = CHROME;

        Configuration.baseUrl = "http://wingoads.com/";
        Configuration.timeout = 90000;
        Configuration.captureJavascriptErrors = true;
        Configuration.startMaximized = true;
        Configuration.screenshots = true;
        Configuration.fastSetValue = false;
        Configuration.reportsFolder = "target/selenide-report";


    }
}
