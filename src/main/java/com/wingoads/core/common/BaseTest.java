package com.wingoads.core.common;

import com.codeborne.selenide.testng.TextReport;
import com.codeborne.selenide.testng.annotations.Report;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.Reporter;
import org.testng.annotations.*;

@Report
@Listeners(TextReport.class)
public class BaseTest {


    private static final Logger log = LogManager.getLogger(BaseTest.class);

    @BeforeClass
    public void beforeClass() {
        log.debug("debug");
        log.error("debug");
        Reporter.log("TestNG_ReportsAndLogs -> This runs once before class", true);

    }


    @AfterClass
    public void afterClass() {
        log.debug("debug");
        log.error("debug");
        Reporter.log("TestNG_ReportsAndLogs -> This runs once after class", true);

    }


    @BeforeTest
    public void beforeTest() {
        log.debug("debug");
        log.error("debug");
        Reporter.log("TestNG_ReportsAndLogs -> This runs before every test", true);
    }

    @AfterTest
    public void afterTest() {
        log.debug("debug");
        log.error("debug");
        Reporter.log("TestNG_ReportsAndLogs -> This runs before every test", true);

    }

    @BeforeMethod
    public void beforeMethod() {
        log.debug("debug");
        log.error("debug");
        Reporter.log("TestNG_ReportsAndLogs -> This runs before every method", true);
    }


    @AfterMethod
    public void afterMethod() {
        log.debug("debug");
        log.error("debug");
        Reporter.log("TestNG_ReportsAndLogs -> This runs after every method", true);
    }


}
