package com.wingoads.pages.wingoads.co;

import com.codeborne.selenide.SelenideElement;
import com.wingoads.pages.wingoads.com.PublisherProfilePage;
import com.wingoads.pages.wingoads.global.BasePage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class PromoPage extends BasePage {

    public static String promoPage = "http://wingoads.co";

    public static SelenideElement enterYourEmailField = $(By.xpath("html/body/section/div[1]/div/div/div/div[1]/div[1]/form/input[1]"));
    public static SelenideElement getStartedButton = $(By.xpath("html/body/section/div[1]/div/div/div/div[1]/div[1]/form/input[3]"));

    public PromoThanksPage registerAsPublisher(String email) {
        registerAsPublisherUser(email);
        return new PromoThanksPage();
    }

    private void registerAsPublisherUser(String login) {
        $(enterYourEmailField).shouldBe(visible).click();
        $(enterYourEmailField).setValue(login);
        $(getStartedButton).click();

    }

    public PromoThanksPage requestToRegisterNewUser(String email) {
        registerAsPublisherUser(email);
        return new PromoThanksPage();

    }
}
