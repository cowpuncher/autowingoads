package com.wingoads.pages.wingoads.com;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.wingoads.pages.wingoads.global.BasePage;
import org.openqa.selenium.By;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ContactPage  extends BasePage{


    public static SelenideElement pleaseSelectADepartmentDropdown = $(By.xpath("html/body/div[6]/div/div/div/div[2]/form/div[1]/div[1]/span/span[1]/span/span[1]"));

    public static SelenideElement yourNameField = $(By.xpath("html/body/div[6]/div/div/div/div[2]/form/div[1]/div[2]/input"));
    public static SelenideElement yourEmailAddressField = $(By.xpath("html/body/div[6]/div/div/div/div[2]/form/div[1]/div[3]/input"));
    public static SelenideElement yourMessageField = $(By.xpath("html/body/div[6]/div/div/div/div[2]/form/div[2]/div/div/textarea"));
    public static SelenideElement sendButton = $(By.xpath("html/body/div[6]/div/div/div/div[2]/form/div[3]/div[2]/input"));




}
