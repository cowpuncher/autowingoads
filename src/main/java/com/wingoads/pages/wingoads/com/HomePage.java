package com.wingoads.pages.wingoads.com;

import com.codeborne.selenide.SelenideElement;
import com.wingoads.pages.wingoads.global.BasePage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exactTextCaseSensitive;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;


/**
 * Created by mario on 7/17/17.
 */
public class HomePage extends BasePage  {


    /**
     * Advertizer / publisher button
     */

    public static SelenideElement advertizerProfileLink =  $(By.xpath("//*[@id=\"navbar\"]/ul/li[4]/a"));
    public static SelenideElement publisherProfileLink = $(By.xpath("//*[@id=\"navbar\"]/ul/li[5]/a"));
    public static SelenideElement logoutLink = $(By.xpath("//*[@id=\"navbar\"]/ul/li[6]/a"));


    /**
     * Promo registration form
     */

    public static SelenideElement enterPasswordPromoField = $(By.xpath("//*[@id=\"promo-password_password\"]"));
    public static SelenideElement repeatPasswordPromoField = $(By.xpath("//*[@id=\"promo-password_password_repeat\"]"));

    public static SelenideElement savePasswordPromoButton = $(By.xpath("//*[@id=\"promo-password-form\"]/div[3]/input"));

    /**
     * forgot password link
     */

    public static SelenideElement forgottenPasswordLink = $(By.xpath("//*[@id=\"signIn\"]/div/div/div[2]/div/div/div[2]/a"));


    /**
     * forgot passord form
     */

    public static SelenideElement forgottenEmailField = $(By.id("reset-password_username"));
    public static SelenideElement resetPasswordButton = $(By.xpath("html/body/div[5]/div/div/div[2]/div/div/form/div[2]/input"));


    /**
     * Validation messages
     */

    public static SelenideElement  validatorDiv = $(By.xpath("help-block wingoads-validator"));
    private String pleaseSupplyYourEmail = "Please supply your E-Mail";
    private String pleaseSupplyValidEmail = "Please supply a valid E-Mail";
    private String pleaseSupplyYourPassword = "Please supply your password";

    private String thisPasswordIsToShort = "This Password is too short (minimum is 3 characters)";

    private String pleaseSupplyConfirmationPassword = "Please supply confirmation password";
    private String passwordDoesNotMatchTheConfirmPassword = "Password does not match the confirm password.";




    /**
     * Complete registration
     */
    public static SelenideElement completeRegistrationAccountConfirmation = $(By.id("signUpLabel"));

    /**
     * SignUp menu
     */
    public static SelenideElement signUpMenu = $(By.xpath(".//*[@id='navbar']/ul/li[5]/a"));

    /**
     * registration submit form
     */

    public static SelenideElement submitRegistrationFormButton = $(By.xpath(".//*[@id='sign-up-form']/div[4]/input"));


    /**
     * Advertizer registration form
     */

    public static SelenideElement advertizerSignUpButton = $(By.xpath(".//*[@id='navbar']/ul/li[5]/ul/li[1]/a"));
    public static SelenideElement advertizerEmailRegistrationForm = $("#sign-up_username");
    public static SelenideElement advertizerPasswordRegistrationForm = $("#sign-up_password");
    public static SelenideElement advertizerConfirmPasswordRegistrationForm = $("#sign-up_password_confirmation");

    /**
     * Publisher registration form
     */

    public static SelenideElement publisherSignUpButton = $(By.xpath("html/body/div[5]/div/div[2]/ul/li[5]/ul/li[2]/a"));
    public static SelenideElement publisherEmailRegistrationForm = $("#sign-up_username");
    public static SelenideElement publisherPasswordRegistrationForm = $("#sign-up_password");
    public static SelenideElement publisherConfirmPasswordRegistrationForm = $("#sign-up_password_confirmation");


    /**
     * Login menu
     */
    public static SelenideElement loginMenu = $(By.xpath(".//*[@id='navbar']/ul/li[4]"));
    public static SelenideElement loginAsPublisherMenu = $(By.xpath(".//*[@id='navbar']/ul/li[4]/ul/li[2]/a"));
    public static SelenideElement loginAsAdvertizerMenu = $(By.xpath(".//*[@id='navbar']/ul/li[4]/ul/li[1]/a"));
    public static SelenideElement loginSubmitButton = $(By.xpath(".//*[@id='sign-in-form']/div[4]/input"));
    public static SelenideElement loginFormKeepMeLoggedInCheckBox = $("#sign-in_remember");

    /**
     * Publisher login form
     */

    public static SelenideElement loginEmailPublisherForm = $("#sign-in_username");
    public static SelenideElement loginPasswordPublisherForm = $("#sign-in_password");


    /**
     * Advertizer login from
     */

    public static SelenideElement loginEmailAdvertizerForm = $("#sign-in_username");
    public static SelenideElement loginPasswordAdvertizerForm = $("#sign-in_password");

    /**
     * Base actions
     */


    public PublisherProfilePage loginAsPublisher(String login, String password) {
        loginAsPublisherUser(login, password);

        return new PublisherProfilePage();

    }


    public AdvertizerProfilePage loginAsAdvertizer(String login, String password) {
        LoginAsAdvertizerMenuUser(login, password);
        return new AdvertizerProfilePage();
    }

    public AdvertizerProfilePage registerAsAdvertizer(String login, String password, String confirm_password) {
        registerAsAdvertizerUser(login, password, confirm_password);

        return new AdvertizerProfilePage();
    }



    public PublisherProfilePage registerAsPublisher(String login, String password, String confirm_password) {
        registerAsPublisherUser(login, password, confirm_password);

        return new PublisherProfilePage();
    }


    public void isEmpty(SelenideElement element){

    }

    public void shouldContain(SelenideElement elemnt, String text)
    {
        $(elemnt).shouldHave(exactTextCaseSensitive("Please supply your E-Mail"));
    }

    public void resetPassword(String email){
        click(forgottenPasswordLink);
        click(resetPasswordButton);
        shouldContain(validatorDiv, pleaseSupplyYourEmail);
        setValue(forgottenEmailField, email);
        shouldContain(validatorDiv, pleaseSupplyValidEmail);
        click(resetPasswordButton);
    }


    private void submitLoginForm() {
        $(loginSubmitButton).click();
    }

    private void registerAsPublisherUser(String login, String password, String confirm_password) {
        $(signUpMenu).shouldBe(visible).click();
        $(publisherSignUpButton).click();
        $(publisherEmailRegistrationForm).setValue(login);
        $(publisherPasswordRegistrationForm).setValue(password);
        $(publisherConfirmPasswordRegistrationForm).setValue(confirm_password);
        $(submitRegistrationFormButton).click();
    }

    private void registerAsAdvertizerUser(String login, String password, String confirm_password) {
        $(signUpMenu).shouldBe(visible).click();
        $(advertizerSignUpButton).click();
        $(advertizerEmailRegistrationForm).setValue(login);
        $(advertizerPasswordRegistrationForm).setValue(password);
        $(advertizerConfirmPasswordRegistrationForm).setValue(confirm_password);
        $(submitRegistrationFormButton).click();
    }

    /**
     * Login action for Publisher
     *
     * @param login
     * @param password
     */

    private void loginAsPublisherUser(String login, String password) {
        $(loginMenu).click();
        $(loginAsPublisherMenu).click();
        $(loginEmailPublisherForm).setValue(login);
        $(loginPasswordPublisherForm).setValue(password);
        submitLoginForm();
    }


    /**
     * Login action for Advertizer
     *
     * @param login
     * @param password
     */

    private void LoginAsAdvertizerMenuUser(String login, String password) {
        $(loginMenu).click();
        $(loginAsAdvertizerMenu).click();
        $(loginEmailAdvertizerForm).setValue(login);
        $(loginPasswordAdvertizerForm).setValue(password);
        submitLoginForm();
    }

    public static void confirmPromoRegistration(String password, String repeatPassword) {
        setValue(enterPasswordPromoField,password);
        setValue(repeatPasswordPromoField, password);
        click(savePasswordPromoButton);

    }
}
