package com.wingoads.pages.wingoads.com;


import com.codeborne.selenide.SelenideElement;
import com.wingoads.pages.wingoads.global.BasePage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

//import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by mario on 7/17/17.
 */
public class AdvertizerProfilePage extends BasePage {


    private static String adv_url = "http://wingoads.com/advertiser/dashboard";

    /**
     * Dashboard page
     */


    /**
     * STATISTICS
     */

    public static SelenideElement statisticsDashboardTitle = $(By.xpath("/html/body/section/div[2]/div/div/ul/li[1]/a"));


    /**
     * Base actions
     */

    /**
     * Should be visible
     *
     * @param element
     */

    public static void shouldBeVisible(SelenideElement element) {
        $(element).waitUntil(visible, 9000);
    }


    /**
     * click actions
     *
     * @param name
     */

//    @Step
    public static void click(SelenideElement name) {
        $(name).waitUntil(visible, 90000).click();

    }


}
