package com.wingoads.pages.wingoads.global;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.url;


/**
 * Created by mario on 7/17/17.
 */
public class BasePage {

    public static SelenideElement acceptThisTermsButtom = $(By.xpath("html/body/div[3]/div/div/div[3]/button"));


    /**
     * Base actions
     */

    /**
     * Should be visible
     *
     * @param element
     */

    public static void shouldBeVisible(SelenideElement element) {
        $(element).waitUntil(visible, 9000);
    }

    /**
     * click actions
     *
     * @param name
     */

    public static void click(SelenideElement name) {
        $(name).waitUntil(visible, 9000).click();

    }

    public static void setValue(SelenideElement element, String value) {
        element.setValue(value);
    }

    public String getCurrentUrl() {
        String currentUrl = url();
        return currentUrl;
    }


}
