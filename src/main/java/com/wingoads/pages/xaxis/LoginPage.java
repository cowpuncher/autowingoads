package com.wingoads.pages.xaxis;

import com.codeborne.selenide.SelenideElement;
import com.wingoads.pages.wingoads.global.BasePage;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends BasePage {

    public static SelenideElement userNameField = $("#username");
    public static SelenideElement userPasswordField = $("#password");
    public static SelenideElement loginButton = $(".btn.btn-primary.btn-md");

    public static void shouldBeVisible(SelenideElement element) {
        $(element).waitUntil(visible, 1000);
    }

    public XaxisPage loginAs(String login, String password) {
        login(login, password);

        return new XaxisPage();
    }

    private void login(String login, String password) {
        $(userNameField).click();
        $(userPasswordField).click();
        $(loginButton).click();
    }


}
