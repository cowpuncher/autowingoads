package com.api.rotator;

import com.wingoads.utils.db.excel.ExcelUtils;
import com.wingoads.utils.db.redis.RedisCleaner;
import io.restassured.response.Response;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static io.restassured.RestAssured.given;


/**
 * Created by dendy on 1/12/17.
 */
public class RotatorTargetingTest {

    /*
    Rotator urls
     */
    private String GET_URL = "http://10.10.10.30/tests/router/getUrls";
    private String ADD_URL = "http://10.10.10.30/tests/router/addUrl";

    private static final String File_Path = "/Users/mario/code/wingoads/tests/src/main/resources/data/rotator/";
    private static final String File_Name = "rotator_targeting_test_data.xlsx";


    /*
    call redid clear method
     */
    RedisCleaner clean = new RedisCleaner();

    @BeforeTest(enabled = false)
    public void setUP() throws Exception {
        System.out.println("Start clean Redis");
        clean.clearRedis();
        System.out.println("Redis clean success ^-^");

    }

    /**
     * @return SearhDataByLocation string
     */
    @DataProvider(name = "SearhDataByLocation")
    public Object[][] dataProvider() {
        try {
            ExcelUtils.setExcelFile(File_Path + File_Name, "SearhDataByLocation");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("File set");
        Object[][] testData = ExcelUtils.getTestData("SearhDataByLocation");
        return testData;
    }

    /**
     * @return AddUrls string
     */
    @DataProvider(name = "AddUrls")
    public Object[][] dataProvider1() {
        try {
            ExcelUtils.setExcelFile(File_Path + File_Name, "AddUrlsToRouter");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("File set");
        Object[][] testData = ExcelUtils.getTestData("AddUrlsToRouter");
        return testData;
    }

    /**
     * get json
     *
     * @param body string
     */
    @Test(enabled = false, dataProvider = "AddUrls")
    public void addUrlsToRoute(String body) {
        Response r = given().contentType("application/json").body(body).then().statusCode(200).when().post(ADD_URL);
        System.out.println(r.getBody().asString());

    }

    /**
     * Create POST request's and asser actual response with expected
     *
     * @param body            string
     * @param expected_result string
     * @throws FileNotFoundException
     */
    @Test(enabled = false, dataProvider = "SearhDataByLocation", dependsOnMethods = "addUrlsToRoute")
    public void searchDataByLocation(String body, String expected_result) throws FileNotFoundException {
        Response r = given().contentType("application/json").body(body).then().statusCode(200).when().post(GET_URL);
        String body_response = r.getBody().asString();
        System.out.println(body_response);
//        Assert.assertEquals(body_response, expected_result);
    }
}
