package com.ui.wingoads.com.publisher;

import com.codeborne.selenide.testng.TextReport;
import com.codeborne.selenide.testng.annotations.Report;
import com.wingoads.core.BrowserConfiguration;
import com.wingoads.pages.wingoads.com.HomePage;
import com.wingoads.pages.wingoads.com.PublisherProfilePage;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.wingoads.pages.wingoads.com.PublisherProfilePage.menuPub;

@Report
@Listeners(TextReport.class)
public class LogInAsPublisher extends BrowserConfiguration {


    String login = "pub@pub.pub";
    String pass = "123";

    @Test
    public void logInAsPublisher() {

        HomePage homePage = open("http://wingoads.com", HomePage.class);
        PublisherProfilePage publisherProfilePage = homePage.loginAsPublisher(login, pass);
        publisherProfilePage.shouldBeVisible(menuPub);


    }
}
