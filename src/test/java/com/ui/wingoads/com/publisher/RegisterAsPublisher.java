package com.ui.wingoads.com.publisher;

import com.codeborne.selenide.testng.TextReport;
import com.codeborne.selenide.testng.annotations.Report;
import com.wingoads.core.BrowserConfiguration;
import com.wingoads.pages.wingoads.com.AdvertizerProfilePage;
import com.wingoads.pages.wingoads.com.HomePage;
import com.wingoads.pages.wingoads.com.PublisherProfilePage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;
import static com.wingoads.pages.wingoads.com.AdvertizerProfilePage.statisticsDashboardTitle;
import static com.wingoads.pages.wingoads.com.HomePage.advertizerConfirmPasswordRegistrationForm;
import static com.wingoads.utils.db.mysql.Mysql.getActivationLink;


@Report
@Listeners(TextReport.class)
public class RegisterAsPublisher extends BrowserConfiguration {
    Random random = new Random();
    int number = random.nextInt(1000);

    String randdoms = String.format("%03d", number);
    String publ = "adv@" + randdoms + ".com";
    String pass = "123";
    String confirm_pass = "123";


    @Test(enabled = true, testName = "testRegisterNewAdvertizer")
    public void testRegisterNewAdvertizer() throws InterruptedException, ClassNotFoundException, ParserConfigurationException, SQLException, IOException {

        HomePage homePage = open(baseUrl, HomePage.class);
        PublisherProfilePage publisherProfilePage = homePage.registerAsPublisher(publ, pass, confirm_pass);

        publisherProfilePage.shouldBeVisible(advertizerConfirmPasswordRegistrationForm);

        String activationLink = getActivationLink(publ);
        open(activationLink);

        AdvertizerProfilePage.shouldBeVisible(statisticsDashboardTitle);


    }
}
