package com.ui.wingoads.com.advertizer;

import com.codeborne.selenide.testng.TextReport;
import com.codeborne.selenide.testng.annotations.Report;
import com.wingoads.core.BrowserConfiguration;
import com.wingoads.pages.wingoads.com.AdvertizerProfilePage;
import com.wingoads.pages.wingoads.com.HomePage;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;
import static com.wingoads.pages.wingoads.com.AdvertizerProfilePage.acceptThisTermsButtom;
import static com.wingoads.pages.wingoads.com.AdvertizerProfilePage.statisticsDashboardTitle;

/**
 * Created by mario on 7/21/17.
 */
@Report
@Listeners(TextReport.class)
public class LogInAsAdvertizer extends BrowserConfiguration {

    String adv = "advpp2pp1p@2adv.adv";
    String pass = "123";
    String adv_url = "http://wingoads.com/advertiser/dashboard";


    @Test(enabled = true, testName = "testRegisterNewAdvertizer")
    public void testRegisterNewAdvertizer() throws InterruptedException {
        HomePage homePage = open(baseUrl, HomePage.class);
        AdvertizerProfilePage advertizerProfilePage = homePage.loginAsAdvertizer(adv, pass);
        advertizerProfilePage.shouldBeVisible(statisticsDashboardTitle);
    }
}
