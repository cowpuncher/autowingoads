package com.ui.wingoads.com.advertizer;

import com.codeborne.selenide.testng.TextReport;
import com.codeborne.selenide.testng.annotations.Report;
import com.wingoads.core.BrowserConfiguration;
import com.wingoads.pages.wingoads.com.AdvertizerProfilePage;
import com.wingoads.pages.wingoads.com.HomePage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.confirm;
import static com.codeborne.selenide.Selenide.open;
import static com.wingoads.pages.wingoads.com.AdvertizerProfilePage.acceptThisTermsButtom;
import static com.wingoads.pages.wingoads.com.AdvertizerProfilePage.statisticsDashboardTitle;
import static com.wingoads.pages.wingoads.com.HomePage.advertizerConfirmPasswordRegistrationForm;
import static com.wingoads.utils.db.mysql.Mysql.getActivationLink;


/**
 * Created by mario on 7/17/17.
 */
@Report
@Listeners(TextReport.class)
public class RegisterAsAdvertizer extends BrowserConfiguration {


    Random random = new Random();
    int number = random.nextInt(1000);

    String randdoms = String.format("%03d", number);
    String adv = "adv@" + randdoms + ".com";
    String pass = "123";
    String confirm_pass = "123";

    @Test(enabled = true, testName = "testRegisterNewAdvertizer")
    public void testRegisterNewAdvertizer() throws InterruptedException, ClassNotFoundException, ParserConfigurationException, SQLException, IOException {

        HomePage homePage = open(baseUrl, HomePage.class);

        AdvertizerProfilePage advertizerProfilePage = homePage.registerAsAdvertizer(adv, pass, confirm_pass);

        advertizerProfilePage.shouldBeVisible(advertizerConfirmPasswordRegistrationForm);

        String activationLink = getActivationLink(adv);
        System.out.println(activationLink);

        String promoActivationLink = activationLink + confirm_pass;
        open(promoActivationLink);

        advertizerProfilePage.click(acceptThisTermsButtom);
        AdvertizerProfilePage.shouldBeVisible(statisticsDashboardTitle);

    }

}
