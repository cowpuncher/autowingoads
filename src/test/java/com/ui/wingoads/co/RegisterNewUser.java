package com.ui.wingoads.co;


import com.wingoads.pages.wingoads.co.PromoPage;
import com.wingoads.pages.wingoads.co.PromoThanksPage;
import com.wingoads.pages.wingoads.com.AdvertizerProfilePage;
import com.wingoads.pages.wingoads.com.HomePage;
import org.testng.annotations.Test;

import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

import static com.codeborne.selenide.Condition.exactTextCaseSensitive;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.wingoads.pages.wingoads.co.PromoThanksPage.registrationInProgressText;
import static com.wingoads.pages.wingoads.co.PromoThanksPage.toCompleteRegistrationClickText;
import static com.wingoads.pages.wingoads.com.AdvertizerProfilePage.statisticsDashboardTitle;
import static com.wingoads.pages.wingoads.com.HomePage.*;
import static com.wingoads.utils.db.mysql.Mysql.getActivationLink;

public class RegisterNewUser {

    Random random = new Random();
    int number = random.nextInt(1000);

    String randdoms = String.format("%03d", number);
    String user = "user@" + randdoms + ".com";


    String toCompleteText = "To complete registration click on the link we have sent you by e-mai";
    String registerInProgress = "REGISTRATION IN PROCESS";

    String urlPromo = "http://wingoads.com/promo/confirm/";

    String password = "123";
    String repeatPassword = "123";


    @Test(enabled = true, testName = "testRegisterNewAdvertizer")
    public void testRegisterNewAdvertizer() throws InterruptedException, ClassNotFoundException, ParserConfigurationException, SQLException, IOException {

        PromoPage  promoPage = open("http://wingoads.com/promo/", PromoPage.class);
        PromoThanksPage promoThanksPage = promoPage.requestToRegisterNewUser(user);
        promoThanksPage.shouldBeVisible(toCompleteRegistrationClickText);
        toCompleteRegistrationClickText.shouldHave(text(toCompleteText));
        promoThanksPage.shouldBeVisible(registrationInProgressText);
        registrationInProgressText.shouldHave(text(registerInProgress));
        String activationLink = urlPromo + getActivationLink(user);
        HomePage homePage = open(activationLink, HomePage.class);
        HomePage.shouldBeVisible(enterPasswordPromoField);
        HomePage.confirmPromoRegistration(password, repeatPassword);
        homePage.shouldBeVisible(logoutLink);
        homePage.shouldBeVisible(advertizerProfileLink);
        homePage.shouldBeVisible(publisherProfileLink);




    }

}